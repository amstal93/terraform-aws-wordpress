# terraform-aws-wordpress

## Provisioning bitnami ami image with terraform in AWS.

## Application versions

* Bitnami AMI: `5.3.2`
* Terraform: `0.12.20`

![coverage](https://gitlab.com/gitlab-org/gitlab-foss/badges/master/coverage.svg?job=coverage)


## Prerequisites

* Amazon AWS account
* Amazon AWS access key
* Amazon aws-cli (optional)
* ssh key pair
* Terraform cli


# Steps

* Clone repository locally
```
git clone git@gitlab.com:enperez/terraform-aws-wordpress.git
cd terraform-aws-wordpress
```

* Update `{repository}/config/aws-credentials` with `access_key` and `access_secret`

* Update the ssh public key in var `public_key` at `{repository}config/vars.tfvars`

* Initialize terraform backend
```
terraform init
```

* Show the desired terraform plan
```
terraform plan -var-file=config/vars.tfvars
```

* Apply terraform plan
```
terraform apply -var-file=config/vars.tfvars
```

# Access

#### Admin console

* Get the public ip/dns address from terraform output vars: `wordpress_public_ip` or `wordpress_public_dns`

* Access to the wp-admin console at: `https://{server_address}/wp-admin`. User credentials are:
```
* Username: user
* Password: Fetch the password from the instance log. Refer to: https://docs.bitnami.com/aws/faq/get-started/find-credentials/
```
*Note*: If the password is not available in the instance log, retreive it at `~/bitnami_credentials` file via ssh connection.

#### SSH

* Connect to the instance by using `bitnami` user at `{server_address}` with the private key generated `{private_pair_key}`
```
ssh bitnami@{server_address} -i {private_pair_key}
```

# How to uninstall

* To uninstall the instance destroy all resources with a single terraform command
```
terraform destroy -var-file=config/vars.tfvars
```
***
# Documentation

Bitnami wordpress stack: https://bitnami.com/stack/wordpress
